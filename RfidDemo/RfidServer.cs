﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RfidDriver
{
    class RfidServer
    {
        /// <summary>
        /// 校验计算 字符串
        /// </summary>
        /// <param name="cmString">需要校验的参数</param>
        /// <returns></returns>
        public string BCC(string cmString)
        {
            int BCC = 0;
            string cmdHex = "";
            // Replace 使用某个字符替换一个字符 ，把空格替换
            string cmdString = cmString.Replace(" ", "");
            string resultStr = "";
            // 两位为一个16进制数所以 /2  -2是因为最后的两位不用校验
            for (int i = 1; i < (cmdString.Length / 2 - 2); i++)
            {
                // 每一次截取两位来异或计算
                cmdHex = cmdString.Substring(i * 2, 2);
                if (i == 1)
                {
                    // 第0位与第一位异或
                    string cmdPrvHex = cmdString.Substring(0 * 2, 2);
                    BCC = (byte)Convert.ToInt32(cmdPrvHex, 16) ^ (byte)Convert.ToInt32(cmdHex, 16);
                }
                else
                {
                    // 异或计算
                    BCC = (byte)BCC ^ (byte)Convert.ToInt32(cmdHex, 16);
                }

                //resultStr += cmdHex;
                if (i == (cmdString.Length / 2 - 2) - 1) // 最后一次循环
                {
                    // 取反
                    BCC = (byte)~BCC;
                    // 获取校验位之前的所有数据
                    resultStr = cmdString.Substring(0, cmdString.Length - 4);
                    Console.WriteLine(resultStr);
                    // 校验结果转换
                    string jy = Convert.ToString(BCC, 16);
                    Console.WriteLine(jy);

                    // 打印帧尾
                    Console.WriteLine(cmdString.Substring((i + 2) * 2, 2));
                    // 拼接校验结果和帧尾
                    resultStr += jy + cmdString.Substring((i + 2) * 2, 2);
                }
            }

            Console.WriteLine(resultStr);
            // ToUpper 转换为大写
            //return Convert.ToString(BCC, 16).ToUpper();
            return resultStr;
        }

        /// <summary>
        /// 校验计算 数组
        /// </summary>
        /// <param name="SerBfr"></param>
        /// <returns></returns>
        public byte BCC(byte[] SerBfr)
        {
            byte BCC = 0, result = 0;

            for (int i = 0; i < (SerBfr[0] - 2); i++)
            {
                BCC ^= SerBfr[i];
            }
            return result = (byte)~BCC;

        }
    }
}
