﻿namespace RfidDriver
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.text_block = new System.Windows.Forms.TextBox();
            this.text_key = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.radio_B = new System.Windows.Forms.RadioButton();
            this.radio_A = new System.Windows.Forms.RadioButton();
            this.button2 = new System.Windows.Forms.Button();
            this.text_messageAlert = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.bt_lj = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.lb_ck = new System.Windows.Forms.Label();
            this.skinEngine1 = new Sunisoft.IrisSkin.SkinEngine();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btn_query = new System.Windows.Forms.Button();
            this.btn_issueCard = new System.Windows.Forms.Button();
            this.text_phone = new System.Windows.Forms.TextBox();
            this.text_studentNumber = new System.Windows.Forms.TextBox();
            this.text_money = new System.Windows.Forms.TextBox();
            this.btn_readValue = new System.Windows.Forms.Button();
            this.btn_writeValue = new System.Windows.Forms.Button();
            this.text_writeValue = new System.Windows.Forms.TextBox();
            this.text_writeData = new System.Windows.Forms.TextBox();
            this.btn_write = new System.Windows.Forms.Button();
            this.btn_read = new System.Windows.Forms.Button();
            this.btn_authKey = new System.Windows.Forms.Button();
            this.btn_cascSelect = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.btn_request = new System.Windows.Forms.Button();
            this.btn_cascAnticoll = new System.Windows.Forms.Button();
            this.btn_auth = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.text_block);
            this.groupBox1.Controls.Add(this.text_key);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.radio_B);
            this.groupBox1.Controls.Add(this.radio_A);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.text_messageAlert);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.bt_lj);
            this.groupBox1.Controls.Add(this.comboBox1);
            this.groupBox1.Controls.Add(this.lb_ck);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(2);
            this.groupBox1.Size = new System.Drawing.Size(417, 123);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "驱动操作";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(212, 59);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(53, 12);
            this.label3.TabIndex = 16;
            this.label3.Text = "目标块：";
            // 
            // text_block
            // 
            this.text_block.Location = new System.Drawing.Point(269, 55);
            this.text_block.Name = "text_block";
            this.text_block.Size = new System.Drawing.Size(21, 21);
            this.text_block.TabIndex = 15;
            this.text_block.Text = "4";
            // 
            // text_key
            // 
            this.text_key.Location = new System.Drawing.Point(75, 54);
            this.text_key.Name = "text_key";
            this.text_key.Size = new System.Drawing.Size(116, 21);
            this.text_key.TabIndex = 14;
            this.text_key.Text = "FF FF FF FF FF FF";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(24, 58);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 12);
            this.label1.TabIndex = 13;
            this.label1.Text = "密  钥:";
            // 
            // radio_B
            // 
            this.radio_B.AutoSize = true;
            this.radio_B.Location = new System.Drawing.Point(358, 56);
            this.radio_B.Name = "radio_B";
            this.radio_B.Size = new System.Drawing.Size(53, 16);
            this.radio_B.TabIndex = 12;
            this.radio_B.Text = "密钥B";
            this.radio_B.UseVisualStyleBackColor = true;
            // 
            // radio_A
            // 
            this.radio_A.AutoSize = true;
            this.radio_A.Checked = true;
            this.radio_A.Location = new System.Drawing.Point(299, 56);
            this.radio_A.Name = "radio_A";
            this.radio_A.Size = new System.Drawing.Size(53, 16);
            this.radio_A.TabIndex = 11;
            this.radio_A.TabStop = true;
            this.radio_A.Text = "密钥A";
            this.radio_A.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(269, 15);
            this.button2.Margin = new System.Windows.Forms.Padding(2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(74, 25);
            this.button2.TabIndex = 11;
            this.button2.Text = "消息显示";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // text_messageAlert
            // 
            this.text_messageAlert.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.text_messageAlert.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.text_messageAlert.Location = new System.Drawing.Point(2, 88);
            this.text_messageAlert.Margin = new System.Windows.Forms.Padding(2);
            this.text_messageAlert.Multiline = true;
            this.text_messageAlert.Name = "text_messageAlert";
            this.text_messageAlert.Size = new System.Drawing.Size(413, 33);
            this.text_messageAlert.TabIndex = 10;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(347, 0);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(68, 21);
            this.button1.TabIndex = 9;
            this.button1.Text = "更换皮肤";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // bt_lj
            // 
            this.bt_lj.Location = new System.Drawing.Point(195, 16);
            this.bt_lj.Margin = new System.Windows.Forms.Padding(2);
            this.bt_lj.Name = "bt_lj";
            this.bt_lj.Size = new System.Drawing.Size(58, 25);
            this.bt_lj.TabIndex = 4;
            this.bt_lj.Text = "连接";
            this.bt_lj.UseVisualStyleBackColor = true;
            this.bt_lj.Click += new System.EventHandler(this.bt_lj_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(75, 18);
            this.comboBox1.Margin = new System.Windows.Forms.Padding(2);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(116, 20);
            this.comboBox1.TabIndex = 0;
            this.comboBox1.MouseClick += new System.Windows.Forms.MouseEventHandler(this.comboBox1_MouseClick);
            // 
            // lb_ck
            // 
            this.lb_ck.AutoSize = true;
            this.lb_ck.Location = new System.Drawing.Point(24, 22);
            this.lb_ck.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lb_ck.Name = "lb_ck";
            this.lb_ck.Size = new System.Drawing.Size(47, 12);
            this.lb_ck.TabIndex = 0;
            this.lb_ck.Text = "串口号:";
            // 
            // skinEngine1
            // 
            this.skinEngine1.@__DrawButtonFocusRectangle = true;
            this.skinEngine1.DisabledButtonTextColor = System.Drawing.Color.Gray;
            this.skinEngine1.DisabledMenuFontColor = System.Drawing.SystemColors.GrayText;
            this.skinEngine1.InactiveCaptionColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.skinEngine1.SerialNumber = "";
            this.skinEngine1.SkinFile = null;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btn_auth);
            this.groupBox3.Controls.Add(this.btn_query);
            this.groupBox3.Controls.Add(this.btn_issueCard);
            this.groupBox3.Controls.Add(this.text_phone);
            this.groupBox3.Controls.Add(this.text_studentNumber);
            this.groupBox3.Controls.Add(this.text_money);
            this.groupBox3.Controls.Add(this.btn_readValue);
            this.groupBox3.Controls.Add(this.btn_writeValue);
            this.groupBox3.Controls.Add(this.text_writeValue);
            this.groupBox3.Controls.Add(this.text_writeData);
            this.groupBox3.Controls.Add(this.btn_write);
            this.groupBox3.Controls.Add(this.btn_read);
            this.groupBox3.Controls.Add(this.btn_authKey);
            this.groupBox3.Controls.Add(this.btn_cascSelect);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.btn_request);
            this.groupBox3.Controls.Add(this.btn_cascAnticoll);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Location = new System.Drawing.Point(0, 123);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(417, 305);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "卡操作";
            // 
            // btn_query
            // 
            this.btn_query.Location = new System.Drawing.Point(339, 219);
            this.btn_query.Name = "btn_query";
            this.btn_query.Size = new System.Drawing.Size(71, 32);
            this.btn_query.TabIndex = 18;
            this.btn_query.Text = "查询";
            this.btn_query.UseVisualStyleBackColor = true;
            this.btn_query.Click += new System.EventHandler(this.btn_query_Click);
            // 
            // btn_issueCard
            // 
            this.btn_issueCard.Location = new System.Drawing.Point(340, 172);
            this.btn_issueCard.Name = "btn_issueCard";
            this.btn_issueCard.Size = new System.Drawing.Size(71, 34);
            this.btn_issueCard.TabIndex = 17;
            this.btn_issueCard.Text = "发卡";
            this.btn_issueCard.UseVisualStyleBackColor = true;
            this.btn_issueCard.Click += new System.EventHandler(this.btn_issueCard_Click);
            // 
            // text_phone
            // 
            this.text_phone.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.text_phone.Location = new System.Drawing.Point(228, 176);
            this.text_phone.Name = "text_phone";
            this.text_phone.Size = new System.Drawing.Size(102, 26);
            this.text_phone.TabIndex = 16;
            this.text_phone.Text = "5853";
            // 
            // text_studentNumber
            // 
            this.text_studentNumber.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.text_studentNumber.Location = new System.Drawing.Point(120, 176);
            this.text_studentNumber.Name = "text_studentNumber";
            this.text_studentNumber.Size = new System.Drawing.Size(102, 26);
            this.text_studentNumber.TabIndex = 15;
            this.text_studentNumber.Text = "1902210065";
            // 
            // text_money
            // 
            this.text_money.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.text_money.Location = new System.Drawing.Point(12, 176);
            this.text_money.Name = "text_money";
            this.text_money.Size = new System.Drawing.Size(102, 26);
            this.text_money.TabIndex = 14;
            this.text_money.Text = "1000";
            // 
            // btn_readValue
            // 
            this.btn_readValue.Location = new System.Drawing.Point(339, 124);
            this.btn_readValue.Name = "btn_readValue";
            this.btn_readValue.Size = new System.Drawing.Size(72, 32);
            this.btn_readValue.TabIndex = 13;
            this.btn_readValue.Text = "读值";
            this.btn_readValue.UseVisualStyleBackColor = true;
            this.btn_readValue.Click += new System.EventHandler(this.btn_readValue_Click);
            // 
            // btn_writeValue
            // 
            this.btn_writeValue.Location = new System.Drawing.Point(258, 124);
            this.btn_writeValue.Name = "btn_writeValue";
            this.btn_writeValue.Size = new System.Drawing.Size(72, 32);
            this.btn_writeValue.TabIndex = 12;
            this.btn_writeValue.Text = "写值";
            this.btn_writeValue.UseVisualStyleBackColor = true;
            this.btn_writeValue.Click += new System.EventHandler(this.btn_writeValue_Click);
            // 
            // text_writeValue
            // 
            this.text_writeValue.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.text_writeValue.Location = new System.Drawing.Point(175, 127);
            this.text_writeValue.Name = "text_writeValue";
            this.text_writeValue.Size = new System.Drawing.Size(78, 26);
            this.text_writeValue.TabIndex = 11;
            this.text_writeValue.Text = "1";
            // 
            // text_writeData
            // 
            this.text_writeData.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.text_writeData.Location = new System.Drawing.Point(2, 81);
            this.text_writeData.Name = "text_writeData";
            this.text_writeData.Size = new System.Drawing.Size(415, 26);
            this.text_writeData.TabIndex = 10;
            this.text_writeData.Text = "00 11 22 33 44 55 66 77 88 99 aa bb cc dd ee ff";
            // 
            // btn_write
            // 
            this.btn_write.Location = new System.Drawing.Point(10, 124);
            this.btn_write.Name = "btn_write";
            this.btn_write.Size = new System.Drawing.Size(72, 32);
            this.btn_write.TabIndex = 9;
            this.btn_write.Text = "写卡";
            this.btn_write.UseVisualStyleBackColor = true;
            this.btn_write.Click += new System.EventHandler(this.btn_write_Click);
            // 
            // btn_read
            // 
            this.btn_read.Location = new System.Drawing.Point(96, 124);
            this.btn_read.Name = "btn_read";
            this.btn_read.Size = new System.Drawing.Size(72, 32);
            this.btn_read.TabIndex = 8;
            this.btn_read.Text = "读卡";
            this.btn_read.UseVisualStyleBackColor = true;
            this.btn_read.Click += new System.EventHandler(this.btn_read_Click);
            // 
            // btn_authKey
            // 
            this.btn_authKey.Location = new System.Drawing.Point(246, 19);
            this.btn_authKey.Name = "btn_authKey";
            this.btn_authKey.Size = new System.Drawing.Size(72, 32);
            this.btn_authKey.TabIndex = 7;
            this.btn_authKey.Text = "验证密码";
            this.btn_authKey.UseVisualStyleBackColor = true;
            this.btn_authKey.Click += new System.EventHandler(this.btn_authKey_Click);
            // 
            // btn_cascSelect
            // 
            this.btn_cascSelect.Location = new System.Drawing.Point(168, 19);
            this.btn_cascSelect.Name = "btn_cascSelect";
            this.btn_cascSelect.Size = new System.Drawing.Size(72, 32);
            this.btn_cascSelect.TabIndex = 6;
            this.btn_cascSelect.Text = "选择";
            this.btn_cascSelect.UseVisualStyleBackColor = true;
            this.btn_cascSelect.Click += new System.EventHandler(this.btn_cascSelect_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(166, 121);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 12);
            this.label2.TabIndex = 5;
            // 
            // btn_request
            // 
            this.btn_request.Location = new System.Drawing.Point(18, 19);
            this.btn_request.Margin = new System.Windows.Forms.Padding(2);
            this.btn_request.Name = "btn_request";
            this.btn_request.Size = new System.Drawing.Size(64, 32);
            this.btn_request.TabIndex = 1;
            this.btn_request.Text = "请求设备";
            this.btn_request.UseVisualStyleBackColor = true;
            this.btn_request.Click += new System.EventHandler(this.btn_request_Click);
            // 
            // btn_cascAnticoll
            // 
            this.btn_cascAnticoll.Location = new System.Drawing.Point(90, 19);
            this.btn_cascAnticoll.Name = "btn_cascAnticoll";
            this.btn_cascAnticoll.Size = new System.Drawing.Size(72, 32);
            this.btn_cascAnticoll.TabIndex = 3;
            this.btn_cascAnticoll.Text = "防碰撞";
            this.btn_cascAnticoll.UseVisualStyleBackColor = true;
            this.btn_cascAnticoll.Click += new System.EventHandler(this.btn_cascAnticoll_Click);
            // 
            // btn_auth
            // 
            this.btn_auth.Location = new System.Drawing.Point(324, 19);
            this.btn_auth.Name = "btn_auth";
            this.btn_auth.Size = new System.Drawing.Size(81, 32);
            this.btn_auth.TabIndex = 19;
            this.btn_auth.Text = "三合一验证";
            this.btn_auth.UseVisualStyleBackColor = true;
            this.btn_auth.Click += new System.EventHandler(this.btn_auth_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(417, 428);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "校园卡门禁系统";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button bt_lj;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label lb_ck;
        private Sunisoft.IrisSkin.SkinEngine skinEngine1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox text_messageAlert;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button btn_authKey;
        private System.Windows.Forms.Button btn_cascSelect;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btn_request;
        private System.Windows.Forms.Button btn_cascAnticoll;
        private System.Windows.Forms.Button btn_write;
        private System.Windows.Forms.Button btn_read;
        private System.Windows.Forms.TextBox text_writeData;
        private System.Windows.Forms.RadioButton radio_B;
        private System.Windows.Forms.RadioButton radio_A;
        private System.Windows.Forms.TextBox text_key;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox text_block;
        private System.Windows.Forms.TextBox text_writeValue;
        private System.Windows.Forms.Button btn_readValue;
        private System.Windows.Forms.Button btn_writeValue;
        private System.Windows.Forms.Button btn_issueCard;
        private System.Windows.Forms.TextBox text_phone;
        private System.Windows.Forms.TextBox text_studentNumber;
        private System.Windows.Forms.TextBox text_money;
        private System.Windows.Forms.Button btn_query;
        private System.Windows.Forms.Button btn_auth;
    }
}

