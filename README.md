# RFID522Driver

#### 介绍
c#RFID rc522驱动程序
该驱动程序封装了s50卡的相关操作，不用去记住繁琐的指令我们已经帮你封装好了。只需要调用相应的接口就能实现，方便快捷。助你更好更快的完成大作业。

2021.6.25 增加中文转换类可以实现中文和16进制的相互转换从而实现读写中文

![输入图片说明](https://images.gitee.com/uploads/images/2021/0518/100311_06247883_7486961.png "屏幕截图.png")

#### 软件架构
```
├─dll
├─IrisSkin4.dll
│  └─Skins
├─RfidDemo
│  ├─bin
│  │  ├─Debug
│  │  │  └─skin
│  │  └─Release
│  ├─obj
│  │  ├─Debug
│  │  │  └─TempPE
│  │  └─Release
│  └─Properties
├─RFIDMifare  此项目就是驱动程序
│  RFIDMifare.csproj
│  RFIDMifare522.cs  卡操作相关接口从这里调用
│  RFIDMifare522Utils.cs    卡操作相关工具类    
│  SerialPortManager.cs    串口驱动程序
│
├─bin
│  ├─Debug
│  │      RFIDMifare.dll
│  │      RFIDMifare.pdb
│  │
│  └─Release
├─obj
│  └─Debug
|
└─Properties
        AssemblyInfo.cs
```


#### 安装教程

1.  将项目中的RFIDMifare文件夹复制到自己的解决方案下
2.  不要忘记从入口项目或者需要使用卡操作的项目引入该驱动
3.  具体操作请详看demo

#### 使用说明

``` c#
//判断串口是否打开
if (mifare522.serialPortManager.IsOpen)
{
    //进行读卡请求
    result = mifare522.PiccRequest();
    if (!result.Status)
    {
        //MessageBox.Show("请求失败，请将卡放置读卡器上");
        UIMessageTip.ShowError("请求失败，请将卡放置读卡器上");
        return;
    }

    //进行防碰撞
    result = mifare522.AntiCollision();
    if (!result.Status)
    {
        //MessageBox.Show("防碰撞失败");
        UIMessageTip.ShowError("防碰撞失败");
        return;
    }

    //读卡成功
    string readCardID = RFIDMifare522Utils.ByteArrayToString(result.Info);
    textCardID.Text = readCardID;
    // 保存卡号
    byte[] card = result.Info;
	
    // 选择
    result = mifare522.Select(result.Info);
    if (!result.Status)
    {
        //MessageBox.Show("防碰撞失败");
        UIMessageTip.ShowError("选择失败");
        return;
    }
	
    // 密码校验
    result = mifare522.AuthenticationKey(card, RFIDMifare522Utils.keyA, 4);
    if (!result.Status)
    {
        //MessageBox.Show("防碰撞失败");
        UIMessageTip.ShowError("秘钥验证失败");
        return;
    }
    // 中文转换为16进制
    string hello = RFIDMifare522Utils.StringToHexString("你好");
     // 16进制转换中文
    Console.WriteLine(RFIDMifare522Utils.HexStringToString(hello));
    // 因为写卡的数据必须为16位一个字符为以为所以需要32个字符不够补左侧0
    result = mifare522.WriteDataBlock(4, RFIDMifare522Utils.HexStringToByteArray(hello.PadLeft(32,'0'))) ;

}
```



#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
